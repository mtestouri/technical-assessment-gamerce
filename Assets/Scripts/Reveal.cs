using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Reveal : MonoBehaviour
{
    public float flipDuration = 0.7f;

    private bool isFlipped = false;
    private bool canFlip = true;

    private static Reveal firstCard;
    private static Reveal secondCard;

    private static int flippedCardCount = 0; // Track the number of flipped cards

    private bool isMatching = false; // Track if a matching process is ongoing

    private static int matchedCardCount = 0; // Count of matched cards
    private static int totalCardCount = 8; // Total number of cards in the scene

    private Quaternion initialRotation;
    private Quaternion targetRotation;


    public GameObject gameDoneImage;
    public GameObject gameCards;
    private void Start()
    {
        initialRotation = transform.rotation;
        targetRotation = initialRotation * Quaternion.Euler(0f, 180f, 0f);

        // Get the reference to the Timer script

        //Reset the number of flipped cards
        flippedCardCount = 0;
        matchedCardCount = 0;

        gameDoneImage.SetActive(false);
gameCards.SetActive(true);
    }

    private void OnMouseDown()
    {
                    Debug.Log("Clicked");

        if (isFlipped) // Check if card is not flipped
        {
            Debug.Log("card is not flipped!");
            return;
        }
        if (!canFlip) // Check if the card can be flipped
        {
            Debug.Log("card can't be flipped!");
            return;
        }
        if (flippedCardCount >= 2) // Check if there are already 2 cards flipped
        {
            Debug.Log("There are already 2 cards flipped!");
            return;
        }
        if (isMatching) // Check if a matching process is ongoing
        {
            Debug.Log("A matching process is already in progress!");
            return;
        }
      

        StartCoroutine(FlipCardAnimation(transform, targetRotation));
        flippedCardCount++;

        if (flippedCardCount == 1)
        {
            firstCard = this;
            firstCard.isFlipped = true;
        }
        else if (flippedCardCount == 2)
        {
            secondCard = this;
            canFlip = false;
            secondCard.isFlipped = true;
            StartCoroutine(DelayedCheckMatch());
        }
    }

    private IEnumerator DelayedCheckMatch()
    {
        isMatching = true; // Set the matching flag to true

        yield return new WaitForSeconds(flipDuration);

        if (firstCard != null && secondCard != null)
        {
            if (firstCard.CompareTag(tag))
            {
                yield return new WaitForSeconds(0.9f);
                Destroy(gameObject);
                Destroy(firstCard.gameObject);

                // Check if all cards have been destroyed
                matchedCardCount += 2;
                if (matchedCardCount == totalCardCount)
                {
                    Debug.Log("Good Job!");
                    // Display the GameDone image
                    gameDoneImage.SetActive(true);
                    gameCards.SetActive(false);
                    matchedCardCount = 0;

                }
            }
            else
            {
                StartCoroutine(FlipBack(firstCard.transform, initialRotation));
                StartCoroutine(FlipBack(secondCard.transform, initialRotation));
            }

            firstCard.isFlipped = false;
            secondCard.isFlipped = false;


            firstCard = null;
            secondCard = null;
            canFlip = true;
            flippedCardCount = 0;


            // Wait for a short delay before resetting flippedCardCount
            yield return new WaitForSeconds(0.8f);
            isMatching = false; // Set the matching flag to false
        }
    }

    private IEnumerator FlipBack(Transform cardTransform, Quaternion targetRotation)
    {
        yield return new WaitForSeconds(flipDuration);
        StartCoroutine(FlipCardAnimation(cardTransform, targetRotation));
    }

    private IEnumerator FlipCardAnimation(Transform cardTransform, Quaternion targetRotation)
{
    Quaternion startRotation = cardTransform.rotation;

    float t = 0f;
    while (t < flipDuration)
    {
        t += Time.deltaTime;
        float normalizedTime = Mathf.Clamp01(t / flipDuration);
        Quaternion newRotation = Quaternion.Lerp(startRotation, targetRotation, normalizedTime);
        cardTransform.rotation = newRotation;
        yield return null;
    }

    cardTransform.rotation = targetRotation;
}
}