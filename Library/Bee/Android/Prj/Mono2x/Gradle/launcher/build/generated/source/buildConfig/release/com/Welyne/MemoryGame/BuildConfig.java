/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.Welyne.MemoryGame;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.Welyne.MemoryGame";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
